import math


def main():
    """Главный метод программы"""

    while True:
        processor()
        if input('Для продолжения введите "y", для выхода "n"') == "n":
            break


def processor():
    """Функция ввода/вычисления/вывода программы"""

    (a, x1, x2, dx, select_function) = get_user_input()
    print('a | x | f(x)')
    print('--------------')
    x = x1
    while x <= x2:
        if select_function == '1':
            result = calc_g(a, x)
        elif select_function == '2':
            result = calc_f(a, x)
        else:
            result = calc_y(a, x)
        print(f'{a} | {x} | {result}')
        print('--------------')
        x += dx


def get_user_input():
    """Функция пользовательского ввода"""

    string = input('Введите a: ')
    a = 0
    try:
        a = float(string)
    except ValueError:
        print('Это не число')
        exit()

    string = input('Введите x1: ')
    x1 = 0
    try:
        x1 = float(string)
    except ValueError:
        print('Это не число')
        exit()

    string = input('Введите x2: ')
    x2 = 0
    try:
        x2 = float(string)
    except ValueError:
        print('Это не число')
        exit()

    string = input('Введите dx: ')
    dx = 0
    try:
        dx = float(string)
    except ValueError:
        print('Это не число')
        exit()

    input('Нажмите Enter что бы выбрать формулу')
    print('1) G = (10 * (-45 * a^2 + 49 * a * x + 6 * x^2)) / (15 * a^2 + 49 * a * x + 24 * x^2)')
    print('2) F = tan(5 * a^2 + 34 * a * x + 45 * x^2)')
    print('3) Y = -1 * asin(7 * a^2 - a * x - 8 * x^2)')
    select_function = input()
    if select_function != '1' and select_function != '2' and select_function != '3':
        print('Ошибка при выборе формулы')
        exit()
    return a, x1, x2, dx, select_function


def calc_g(a, x):
    """Функция подсчета формулы G"""

    try:
        return (10 * (-45 * a ** 2 + 49 * a * x + 6 * x ** 2)) / (
                15 * a ** 2 + 49 * a * x + 24 * x ** 2)
    except ZeroDivisionError:
        return 'Попытка деления на ноль'


def calc_f(a, x):
    """Функция подсчета формулы F"""

    return math.tan(math.radians(5 * a ** 2 + 34 * a * x + 45 * x ** 2))


def calc_y(a, x):
    """Функция подсчета формулы Y"""

    try:
        return -1 * math.asin(math.radians(7 * a ** 2 - a * x - 8 * x ** 2))
    except ValueError:
        return 'Ошибка'


main()
